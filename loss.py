import torch
import torch.nn.functional as F
import torch.nn as nn

import hard_mining


def imbalance(a):
    freq = torch.sum(a, axis=0).float()
    (idx,) = torch.nonzero(freq, as_tuple=True)
    freq[idx] = freq.max() / freq[idx] - 1
    return freq


"""
def triplet_ranking(x_a, x_hard, m_j):
    d_pos = torch.abs(x_a - x_hard[0])
    d_neg = x_a - x_hard[1]
    return F.relu(m_j + d_pos + d_neg)


def meshgrid_triplet_ranking(x, m_j):
    d_pos = torch.abs(x[0] - x[1])
    d_neg = x[0] - x[2]
    return F.relu(m_j + d_pos + d_neg)
"""


def loss_crl(p, a, weight, minority=None, k=10, rho=0.5, m_j=0.5):
    if minority is None:
        minority = hard_mining.attributes2minotiry(a, rho=rho)

    a_m = a[:, minority]
    p_m = p[:, minority]
    w_m = weight[minority]

    (
        anchor_p,
        anchor_mask,
        hard_pos_p,
        hard_pos_mask,
        hard_neg_p,
        hard_neg_mask,
    ) = hard_mining.get_hard_sample(a_m, p_m, k)

    a_idx = torch.arange(0, anchor_p.shape[0], dtype=int)
    c_idx = torch.arange(0, anchor_p.shape[1], dtype=int)
    h_idx = torch.arange(0, k, dtype=int)

    a, j, h = torch.meshgrid(a_idx, c_idx, h_idx)
    d_pos_mask = anchor_mask[a, j] & hard_pos_mask[h, j]
    d_neg_mask = anchor_mask[a, j] & hard_neg_mask[h, j]
    d_pos = torch.abs(anchor_p[a, j] - hard_pos_p[h, j]) * d_pos_mask
    d_neg = (anchor_p[a, j] - hard_neg_p[h, j]) * d_neg_mask

    j, a, p, n = torch.meshgrid(c_idx, a_idx, h_idx, h_idx)
    triplet_mask = d_pos_mask[a, j, p] & d_neg_mask[a, j, n]
    triplet = F.relu((d_pos[a, j, p] - d_neg[a, j, n] + m_j) * triplet_mask)

    triplet_byclass = triplet.view(anchor_p.shape[1], -1).sum(axis=1)
    N_byclass = triplet_mask.view(anchor_p.shape[1], -1).sum(axis=1).float()

    loss = (triplet_byclass / N_byclass * w_m).mean()

    return loss


def loss_bln(
    p, a, eta, omega=None, minority=None, k=10, rho=0.5, m_j=0.5, multiclass=False
):
    if omega is None:
        omega = imbalance(a)
    alpha = torch.clamp(eta * omega, min=0.0, max=1.0).detach().clone()

    l_crl = loss_crl(p, a, weight=alpha, minority=minority, k=k, rho=rho, m_j=m_j)

    if multiclass:
        l_ce = F.binary_cross_entropy(p, a, weight=(1 - alpha))
    else:
        l_ce = F.nll_loss(p, a, weight=(1 - alpha))

    l_bln = l_crl + l_ce

    return l_bln, l_ce, l_crl


class BLNLoss(nn.Module):
    def __init__(self, eta=0.01, k=10, rho=0.5, m_j=0.5, multiclass=False):
        super(BLNLoss, self).__init__()

        self.eta = eta
        self.k = k
        self.rho = rho
        self.m_j = m_j
        self.multiclass = multiclass

    def forward(self, prediction, target, omega=None, minority=None):
        return loss_bln(
            prediction,
            target,
            eta=self.eta,
            omega=omega,
            minority=minority,
            k=self.k,
            rho=self.rho,
            m_j=self.m_j,
            multiclass=self.multiclass,
        )
