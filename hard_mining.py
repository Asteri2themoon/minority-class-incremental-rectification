import torch


def attributes2minotiry(a, rho=0.5):
    freq = a.sum(axis=0)
    n_bs = freq.sum()

    (freq_nz,) = freq.nonzero(as_tuple=True)
    freq_j = freq_nz[torch.argsort(freq[freq_nz])]

    threshold = int(n_bs * rho)

    minority = freq_j[torch.cumsum(freq[freq_j], dim=0) <= threshold]

    return minority


def get_hard_sample(a, p, k):
    anchor_len = a.sum(axis=0).max().int()
    anchor, anchor_idx = a.sort(axis=0, descending=True)

    anchor_mask = anchor[:anchor_len, :].bool()

    anchor_p = p.gather(dim=0, index=anchor_idx)[:anchor_len, :]

    pos_mask = torch.masked_fill(p, a == 0.0, 2.0)
    neg_mask = torch.masked_fill(p, a == 1.0, -1.0)

    hard_pos_p = torch.topk(pos_mask, k, dim=0, largest=False)[0]
    hard_neg_p = torch.topk(neg_mask, k, dim=0, largest=True)[0]

    hard_pos_mask = hard_pos_p != 2.0
    hard_neg_mask = hard_neg_p != -1.0

    return (
        anchor_p,
        anchor_mask,
        hard_pos_p,
        hard_pos_mask,
        hard_neg_p,
        hard_neg_mask,
    )
