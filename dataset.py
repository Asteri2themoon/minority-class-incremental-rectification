import torch
from torch.utils.data import Dataset, DataLoader, SubsetRandomSampler

from torchvision import transforms, utils

import PIL

import numpy as np
import pandas as pd

import git

import hard_mining

import glob
import zipfile
import json
import time
import os
import requests
import argparse
import urllib3
import multiprocessing as mp


def _fixbrokent(dataset_output):
    print("checking dataset sanity")
    errors = 0

    base_dir = os.path.join(dataset_output, "tmp")
    images = "images"
    jsonfile_train = "train.json"
    jsonfile_validation = "validation.json"
    filename_train_meta = os.path.join(base_dir, jsonfile_train)
    filename_validation_meta = os.path.join(base_dir, jsonfile_validation)

    print("loading json")
    with open(filename_train_meta, "r") as file_train_meta:
        train_meta = json.load(file_train_meta)
    with open(filename_validation_meta, "r") as file_validation_meta:
        validation_meta = json.load(file_validation_meta)

    print("extract meta-data")
    image_map = {}
    for image in train_meta["annotations"]:
        image_map[int(image["imageId"])] = {
            "labels": [int(c) for c in image["labelId"]]
        }
    for image in validation_meta["annotations"]:
        image_map[int(image["imageId"])] = {
            "labels": [int(c) for c in image["labelId"]]
        }

    for image in train_meta["images"]:
        image_map[int(image["imageId"])]["url"] = image["url"]
    for image in validation_meta["images"]:
        image_map[int(image["imageId"])]["url"] = image["url"]

    print("check pictures")
    files = glob.glob(os.path.join(dataset_output, images, "*.jpg"))
    for i, img_name in enumerate(files):
        print(f"{i+1:>7}/{len(files)}", end="\r")
        try:
            PIL.Image.open(img_name)
        except OSError:
            f_id = int(os.path.splitext(os.path.split(img_name)[1])[0])
            print(f"\npicture id {f_id} is brocken (url={image_map[f_id]['url']})")
            _download(img_name, image_map[f_id]["url"])
            errors += 1
    print(f"\n{errors} has been found")
    return errors


def _download(file_name, url):
    img_data = requests.get(url).content
    with open(file_name, "wb") as handler:
        handler.write(img_data)


def _worker(q):
    while True:
        job = q.get()
        if job is None:
            break
        success = False
        while not success:
            try:
                _download(job["file_name"], job["url"])
            except:
                time.sleep(30)
            else:
                success = True


class IMaterialDataset(Dataset):
    """IMaterial dataset."""

    ground_truth = "ground_truth.npy"
    images = "images"

    def __init__(self, root_dir, mining=False, transform=None, download=False):
        self.root_dir = root_dir
        self.mining = mining

        if download and (not os.path.exists(root_dir)):
            self.download(root_dir)

        self.gt = np.load(os.path.join(self.root_dir, self.ground_truth))
        if transform is not None:
            self.transform = transform
        else:
            self.transform = transforms.Compose(
                [transforms.Resize((256, 256)), transforms.ToTensor(),]
            )

    @staticmethod
    def download(dataset_output):
        mp.set_start_method("spawn")

        dataset_images = os.path.join(dataset_output, "images")
        base_dir = os.path.join(dataset_output, "tmp")
        url = "https://github.com/suraj2596/iMaterialist-Challenge-Fashion-at-FGVC5.git"
        zipfile_train = (
            "iMaterialist-Challenge-Fashion-at-FGVC5/datasets/train.json.zip"
        )
        zipfile_validation = (
            "iMaterialist-Challenge-Fashion-at-FGVC5/datasets/validation.json.zip"
        )
        jsonfile_train = "train.json"
        jsonfile_validation = "validation.json"

        try:
            os.makedirs(base_dir)
        except OSError:
            pass

        print(f"clonning git repo {url}")
        try:
            git.Git(base_dir).clone(url)
        except git.exc.GitCommandError:
            pass

        filename_train_meta = os.path.join(base_dir, jsonfile_train)
        filename_validation_meta = os.path.join(base_dir, jsonfile_validation)

        print("unzipping")
        with zipfile.ZipFile(os.path.join(base_dir, zipfile_train), "r") as zip_ref:
            zip_ref.extractall(base_dir)
        with zipfile.ZipFile(
            os.path.join(base_dir, zipfile_validation), "r"
        ) as zip_ref:
            zip_ref.extractall(base_dir)

        print("loading json")
        with open(filename_train_meta, "r") as file_train_meta:
            train_meta = json.load(file_train_meta)
        with open(filename_validation_meta, "r") as file_validation_meta:
            validation_meta = json.load(file_validation_meta)

        print("extract meta-data")
        image_map = {}
        for image in train_meta["annotations"]:
            image_map[int(image["imageId"])] = {
                "labels": [int(c) for c in image["labelId"]]
            }
        for image in validation_meta["annotations"]:
            image_map[int(image["imageId"])] = {
                "labels": [int(c) for c in image["labelId"]]
            }

        for image in train_meta["images"]:
            image_map[int(image["imageId"])]["url"] = image["url"]
        for image in validation_meta["images"]:
            image_map[int(image["imageId"])]["url"] = image["url"]

        del train_meta
        del validation_meta

        try:
            os.makedirs(dataset_output)
        except OSError:
            pass

        print("save ground truth")
        attributes = 228
        ground_truth = np.zeros((len(image_map.keys()), attributes)).astype(bool)
        for i_id, i_labels in image_map.items():
            for l in i_labels["labels"]:
                ground_truth[i_id - 1, l - 1] = True

        np.save(os.path.join(dataset_output, "ground_truth"), ground_truth.astype(bool))
        del ground_truth

        print("download images")
        try:
            os.makedirs(dataset_images)
        except OSError:
            pass

        q = mp.Queue()

        N_images = len(image_map)
        N_workers = 32

        for i_id, i_labels in image_map.items():
            q.put(
                {
                    "file_name": os.path.join(dataset_images, f"{i_id}.jpg"),
                    "url": i_labels["url"],
                }
            )
        for _ in range(N_workers):
            q.put(None)

        process = []
        for _ in range(N_workers):
            p = mp.Process(target=_worker, args=(q,))
            p.start()
            process.append(p)

        while not q.empty():
            print(f"download: {N_images-q.qsize()+N_workers+1}/{N_images}", end="\r")
            time.sleep(0.1)
        print()

        for p in process:
            p.join()

        print("download completed!")

        errors = -1
        while errors != 0:
            errors = _fixbrokent(dataset_output)

    def __len__(self):
        return self.gt.shape[0]

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.item()

        img_name = os.path.join(self.root_dir, self.images, f"{idx+1}.jpg")
        try:
            image = PIL.Image.open(img_name)
            if self.transform:
                image = self.transform(image)
        except OSError:
            image = None  # torch.randn((3, 256, 256))
            f = open(img_name, "r").read()
            print(f"\nganged\n{f}")
            exit(0)

        a = self.gt[idx, :]

        return (image, torch.from_numpy(a))


class CustomBatch:
    def __init__(self, data):
        transposed_data = list(zip(*data))
        self.img = torch.stack(transposed_data[0], 0)
        self.att = torch.stack(transposed_data[1], 0).float()
        self.omega = self.imbalance(self.att)
        self.minority = self.attributes2minotiry(self.att)

    @staticmethod
    def max_attr(a):
        return a.sum(axis=0).max().int()

    @staticmethod
    def imbalance(a):
        freq = torch.sum(a, axis=0).float()
        (idx,) = torch.nonzero(freq, as_tuple=True)
        freq[idx] = freq.max() / freq[idx] - 1
        return freq

    @staticmethod
    def attributes2minotiry(a, rho=0.5):
        freq = a.sum(axis=0)
        n_bs = freq.sum()

        (freq_nz,) = freq.nonzero(as_tuple=True)
        freq_j = freq_nz[torch.argsort(freq[freq_nz])]

        threshold = int(n_bs * rho)
        minority = freq_j[torch.cumsum(freq[freq_j], dim=0) < threshold]

        return minority

    # custom memory pinning method on custom type
    def pin_memory(self):
        self.img = self.img.pin_memory()
        self.att = self.att.pin_memory()
        self.omega = self.omega.pin_memory()
        self.minority = self.minority.pin_memory()
        return self


def collate_wrapper(batch):
    return CustomBatch(batch)


def get_dataloader(
    dataset: Dataset, batch_size: int, num_workers: int = 8, split=None, seed: int = 42
):
    N = len(dataset)
    if split is None:
        split = (N,)
    else:
        assert np.sum(split) == N
        split = split

    split = np.cumsum((0,) + split)

    indices = np.arange(N).astype(int)

    if seed is not None:
        np.random.seed(seed)
        np.random.shuffle(indices)

    dataloaders = []
    for begin, end in zip(split[:-1], split[1:]):
        sampler = SubsetRandomSampler(indices[begin:end])
        dataloader = DataLoader(
            dataset,
            batch_size=batch_size,
            sampler=sampler,
            num_workers=num_workers,
            collate_fn=collate_wrapper,
            pin_memory=True,
        )
        dataloaders.append(dataloader)

    return tuple(dataloaders)
