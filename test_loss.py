import torch

import loss
import hard_mining


import unittest


class TestLoss(unittest.TestCase):
    gt = torch.tensor(
        [
            [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0],
            [0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0],
            [0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1],
        ],
        dtype=bool,
    ).t_()
    pred = (
        torch.tensor(
            [
                [1, 0, 8, 7, 9, 3, 5, 3, 5, 4, 7, 9, 2, 2, 1, 7],
                [0, 3, 4, 5, 1, 5, 5, 9, 3, 2, 4, 1, 9, 5, 0, 8],
                [8, 8, 1, 2, 3, 4, 7, 3, 9, 5, 2, 7, 4, 2, 0, 1],
            ],
            dtype=float,
        ).t_()
        / 10
    )
    k = 5
    rho = 0.5
    tho = 0.01
    m_j = 0.5

    minority = torch.tensor([0, 1])
    anchors = torch.tensor([[0, 2, 10, 13, 0, 0], [1, 5, 7, 9, 13, 14]])
    anchors_mask = torch.tensor(
        [[1, 1, 1, 1, 0, 0], [1, 1, 1, 1, 1, 1]], dtype=bool
    ).t_()
    omega_imb = torch.tensor([1.75, 5 / 6, 0])

    hard_pos = torch.tensor([[0, 13, 10, 2, 0], [14, 9, 1, 5, 13]])
    hard_pos_mask = torch.tensor([[1, 1, 1, 1, 0], [1, 1, 1, 1, 1]], dtype=bool).t_()

    hard_neg = torch.tensor([[4, 11, 3, 15, 6], [12, 15, 3, 6, 2]])
    hard_neg_mask = torch.tensor([[1, 1, 1, 1, 1], [1, 1, 1, 1, 1]], dtype=bool).t_()

    real_pos = torch.cat(
        [pred[hard_pos[0], 0].view(1, -1), pred[hard_pos[1], 1].view(1, -1),], dim=0,
    ).t_()
    real_neg = torch.cat(
        [pred[hard_neg[0], 0].view(1, -1), pred[hard_neg[1], 1].view(1, -1),], dim=0,
    ).t_()
    real_anc = torch.cat(
        [pred[anchors[0], 0].view(1, -1), pred[anchors[1], 1].view(1, -1),], dim=0,
    ).t_()

    def test_minority(self):
        assert (
            hard_mining.attributes2minotiry(self.gt) - self.minority
        ).abs().sum() == 0

    def test_hard_mining(self):
        a = self.gt.float()[:, self.minority]
        p = self.pred[:, self.minority]
        (
            anchors_p,
            anchors_mask,
            hard_pos_p,
            hard_pos_mask,
            hard_neg_p,
            hard_neg_mask,
        ) = hard_mining.get_hard_sample(a, p, self.k)

        assert (hard_pos_mask ^ self.hard_pos_mask).sum() == 0
        assert ((hard_pos_p - self.real_pos) * self.hard_pos_mask).abs().sum() == 0

        assert (hard_neg_mask ^ self.hard_neg_mask).sum() == 0
        assert ((hard_neg_p - self.real_neg) * self.hard_neg_mask).abs().sum() == 0

        anchors_p = torch.sort(anchors_p * self.anchors_mask, dim=0)[0]
        real_anc = torch.sort(self.real_anc * self.anchors_mask, dim=0)[0]

        assert (anchors_mask ^ self.anchors_mask).sum() == 0
        assert ((anchors_p - real_anc) * self.anchors_mask).abs().sum() == 0

    def test_imbalance(self):
        assert (self.omega_imb - loss.imbalance(self.gt)).abs().sum() < 1e-7

    def test_loss_crl(self):
        total = torch.zeros(2)
        T = torch.zeros(2)

        for j in self.minority:
            for i, a in enumerate(self.anchors[j]):
                for p in range(self.k):
                    for n in range(self.k):
                        if (
                            self.anchors_mask[i, j]
                            and self.hard_pos_mask[p, j]
                            and self.hard_neg_mask[n, j]
                        ):
                            total[j] += max(
                                abs(self.pred[a, j] - self.pred[self.hard_pos[j, p], j])
                                - (self.pred[a, j] - self.pred[self.hard_neg[j, n], j])
                                + self.m_j,
                                0,
                            )
                            T[j] += 1

        real_loss = (total / T).mean()

        lib_loss = loss.loss_crl(
            p=self.pred,
            a=self.gt.float(),
            weight=torch.ones(2),
            k=self.k,
            rho=self.rho,
            m_j=self.m_j,
        )
        assert (real_loss - lib_loss).abs() < 1e-6


if __name__ == "__main__":
    unittest.main()
