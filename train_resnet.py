import torch
import torch.optim as optim
import torch.nn as nn
import pandas as pd
import numpy as np

import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt

from torchtrain import Training, Settings, HParams

from metrics import LossMetric, ClassificationMetric
from models import ResNet34
from loss import BLNLoss
from dataset import IMaterialDataset, get_dataloader

import os
import math
import time


class TrainResNet(Training):
    __scope__ = "training.resnet"

    def __init__(self, **kwargs):
        super(TrainResNet, self).__init__(**kwargs)

        self.metrics.add_metrics(
            ["train", "validate", "test"], LossMetric(device=self.device)
        )
        self.metrics.add_metrics(
            ["validate", "test"],
            ClassificationMetric(num_class=228, device=self.device),
        )

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        with hparams.scope(self.__scope__) as p:
            self.batch_size = p.batch_size
            self.epoch = p.epoch

        self.model = ResNet34(**hparams.params)

        self.model = self.model.to(self.device)

        with hparams.scope(self.__scope__) as p:
            self.optimizer = optim.SGD(
                self.model.parameters(),
                lr=p.lr,
                momentum=p.momentum,
                weight_decay=p.decay,
            )
            print(f"lr:{p.lr}")
            print(f"eta:{p.eta}")
            self.loss = BLNLoss(eta=p.eta, k=p.k, multiclass=True)
            # self.loss = nn.BCELoss()

        # setup data loader
        with hparams.scope(self.__scope__) as p:
            (
                self.train_loader,
                self.validation_loader,
                self.test_loader,
            ) = get_dataloader(
                IMaterialDataset(dataset_path),
                batch_size=self.batch_size,
                num_workers=self.num_workers,
                split=tuple(p.split),
            )

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, epoch, batch):
        t0 = time.time()
        optimizer.zero_grad()

        x = data.img.to(self.device)
        a = data.att.to(self.device)
        omega = data.omega.to(self.device)
        minority = data.minority.to(self.device)

        pred = model(x)
        l, _, _ = loss(pred, a, omega=omega, minority=minority)

        l.backward()
        optimizer.step()

        res = {"loss": l, "prediction": pred, "ground_truth": a}

        return res

    # validate on one batch
    def validate_batch(self, data, model, optimizer, loss, epoch, batch):
        x = data.img.to(self.device)
        a = data.att.to(self.device)

        pred = model(x)
        l, _, _ = loss(pred, a)

        return {"loss": l, "prediction": pred, "ground_truth": a}

    # test on one batch
    def test_batch(self, data, model, optimizer, loss, batch):
        x = data.img.to(self.device)
        a = data.att.to(self.device)

        pred = model(x)
        l, _, _ = loss(pred, a)

        return {"loss": l, "prediction": pred, "ground_truth": a}


with Settings.default.scope(TrainResNet.__scope__) as hparams:
    hparams.batch_size = 256
    hparams.epoch = 200
    hparams.lr = 1e-1
    hparams.decay = 5e-4
    hparams.momentum = 0.9
    hparams.eta = 1e-4
    hparams.k = 10
    hparams.dataset_workers = 1
    hparams.split = (924544, 10000, 80000)
