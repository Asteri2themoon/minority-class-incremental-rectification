import unittest
import os

import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader, SubsetRandomSampler

import dataset


class TestDataset(unittest.TestCase):
    base_dir = "unittest"

    def test_dataset(self):
        d = dataset.IMaterialDataset(
            os.path.join(self.base_dir, "imaterial"), download=True
        )

        train_loader, valid_loader, test_loader = dataset.get_dataloader(
            d, batch_size=4, split=(16, 8, 8)
        )

        assert len(train_loader) == 4
        assert len(valid_loader) == 2
        assert len(test_loader) == 2

        for data in train_loader:
            assert data["image"].shape == (4, 3, 256, 256)
            assert data["attributes"].shape == (4, 228)
        for data in valid_loader:
            assert data["image"].shape == (4, 3, 256, 256)
            assert data["attributes"].shape == (4, 228)
        for data in test_loader:
            assert data["image"].shape == (4, 3, 256, 256)
            assert data["attributes"].shape == (4, 228)

        sum_train = np.sum([data["image"][:, :, 0, 0].numpy() for data in train_loader])
        sum_valid = np.sum([data["image"][:, :, 0, 0].numpy() for data in valid_loader])
        sum_test = np.sum([data["image"][:, :, 0, 0].numpy() for data in test_loader])

        train_loader, valid_loader, test_loader = dataset.get_dataloader(
            d, batch_size=4, split=(16, 8, 8)
        )

        sum_train2 = np.sum(
            [data["image"][:, :, 0, 0].numpy() for data in train_loader]
        )
        sum_valid2 = np.sum(
            [data["image"][:, :, 0, 0].numpy() for data in valid_loader]
        )
        sum_test2 = np.sum([data["image"][:, :, 0, 0].numpy() for data in test_loader])

        assert sum_train == sum_train2
        assert sum_valid == sum_valid2
        assert sum_test == sum_test2


if __name__ == "__main__":
    unittest.main()
