import torch
import torch.nn as nn
import torch.nn.functional as F

import torchvision.models as models

from torchtrain import DeepModel, Settings, HParams

import math
import warnings
from typing import Type, Any
import importlib


class ResNet34(DeepModel):
    __scope__ = "model.resnet34"

    with Settings.default.scope(__scope__) as hparams:
        hparams.output_dim = 228

    def __init__(self, **kwargs):
        super(ResNet34, self).__init__()

        with self.scope() as p:
            self.model = models.resnet34(pretrained=True)
            if p.output_dim != 1000:
                self.model.fc = nn.Linear(512, p.output_dim)

    def forward(self, x):
        return torch.sigmoid(self.model(x))
